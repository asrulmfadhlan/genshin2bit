# ONEDRIVE 
<H2> Menampilkan use case table untuk produk digitalnya

### Tabel Users
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat mendaftar akun baru dengan username dan password | 100 |
| 2 | User dapat masuk ke akun OneDrive menggunakan username dan password | 100 |
| 3 | User dapat mengubah username dan password akun | 90 |
| 4 | User dapat menghapus akun OneDrive | 80 |
| 5 | User dapat melihat informasi profil akun | 90 |
| 6 | User dapat mengubah informasi profil akun seperti nama, alamat email, dan foto profil | 90 |

### Tabel Files
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat mengunggah file ke OneDrive | 100 |
| 2 | User dapat menghapus file dari OneDrive | 100 |
| 3 | User dapat mengunduh file dari OneDrive | 100 |
| 4 | User dapat membagikan file dengan pengguna lain | 90 |
| 5 | User dapat membuat salinan file | 80 |
| 6 | User dapat mencari file berdasarkan nama atau tipe file | 90 |

### Tabel Folders
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat membuat folder baru di OneDrive | 100 |
| 2 | User dapat menghapus folder dari OneDrive | 100 |
| 3 | User dapat mengubah nama folder di OneDrive | 90 |
| 4 | User dapat mengatur hak akses folder untuk pengguna lain | 90 |
| 5 | User dapat melihat isi folder dan file yang ada di dalamnya | 90 |
| 6 | User dapat memindahkan file atau folder ke lokasi lain di OneDrive | 90 |

### Tabel SharedFiles
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat menerima file yang dibagikan oleh pengguna lain | 100 |
| 2 | User dapat mengakses file yang dibagikan dengan hak akses yang diberikan | 100 |
| 3 | User dapat menghapus akses berbagi file | 90 |
| 4 | User dapat memberikan hak akses berbagi file kepada pengguna lain | 90 |
| 5 | User dapat melihat daftar file yang dibagikan olehnya kepada pengguna lain | 90 |
| 6 | User dapat melihat daftar file yang dibagikan oleh pengguna lain kepada dirinya | 90 |

### Tabel ActivityLog
| No | Use Case | Score |
| --- | --- | --- |
| 1 | Sistem mencatat aktivitas pengguna seperti mengunggah, menghapus, atau mengunduh file | 100 |
| 2 | User dapat melihat log aktivitas terkini di akun OneDrive | 90 |
| 3 | User dapat menghapus log aktivitas tertentu di akun OneDrive | 90 |
| 4 | User dapat mencari log aktivitas berdasarkan tanggal atau tipe aktivitas | 90 |
| 5 | Sistem memberikan notifikasi kepada pengguna tentang aktivitas terbaru di akun OneDrive | 80 |
| 6 | User dapat mengatur preferensi notifikasi untuk aktivitas di akun OneDrive | 80 |

<H2> 2

#### Koneksi Ke database
```php
$host = "localhost";
$username = "root";
$password = "";
$database = "onedrive";

function create_connection() {
    global $host, $username, $password, $database;
    $conn = new mysqli($host, $username, $password, $database);
    if ($conn->connect_error) {
        die("Koneksi ke database gagal: " . $conn->connect_error);
    }
    return $conn;
}
```
### RESTful API
#### CREATE 
```php
$app->post('/user', function (Request $request, Response $response, array $args) {
    $userName = $request->getParam('userName');
    $userpass = $request->getParam('userPass');

    $connOneDrive = create_connection();

    $userSql = "INSERT INTO user (name, password) VALUES (?, ?)";
    $fileStmt = $connOneDrive->prepare($userSql);
    $fileStmt->bind_param("si", $userName, $userPass);

    $isUserCreated = $userStmt->execute();

    if ($isFileInserted) {
        $data = [
            'message' => 'User inserted successfully.'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: Failed to Creat User.'
        ];
        return $response->withJson($data, 500);
    }

    $userStmt->close();
    $connOneDrive->close();
});

```
#### READ
```php
$app->get('/files', function (Request $request, Response $response, array $args) {
    $conn = create_connection();
    $sql = "SELECT * FROM files";
    $result = $conn->query($sql);
    $files = [];

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $files[] = $row;
        }
        return $response->withJson($files, 200);
    } else {
        $data = [
            'message' => 'Data file tidak ditemukan'
        ];
        return $response->withJson($data, 404);
    }

    $conn->close();
});
```
```php
$app->get('/folders', function (Request $request, Response $response, array $args) {
    $conn = create_connection();
    $sql = "SELECT * FROM folders";
    $result = $conn->query($sql);
    $folders = [];

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $folders[] = $row;
        }
        return $response->withJson($folders, 200);
    } else {
        $data = [
            'message' => 'Data folder tidak ditemukan'
        ];
        return $response->withJson($data, 404);
    }

    $conn->close();
});
```
#### UPDATE
```php
$app->put('/files/{id}', function (Request $request, Response $response, array $args) {
    $file_id = $args['id'];
    $name = $request->getParam('name');
    $size = $request->getParam('size');

    $conn = create_connection();
    $sql = "UPDATE files SET name=?, size=? WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sii", $name, $size, $file_id);

    if ($stmt->execute()) {
        $data = [
            'message' => 'Data file berhasil diperbarui'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: ' . $stmt->error
        ];
        return $response->withJson($data, 500);
    }

    $stmt->close();
    $conn->close();
});
````
#### DELETE
```php
$app->delete('/folders/{id}', function (Request $request, Response $response, array $args) {
    $folder_id = $args['id'];

    $conn = create_connection();
    $sql = "DELETE FROM folders WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $folder_id);

    if ($stmt->execute()) {
        $data = [
            'message' => 'Data folder berhasil dihapus'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: ' . $stmt->error
        ];
        return $response->withJson($data, 500);
    }

    $stmt->close();
    $conn->close();
});
```

## 4
1. Built-in Function: Regex (`preg_match`)
   Penggunaan Regex untuk memvalidasi format email pada kolom email dari data pemain. Jika format email tidak sesuai, maka akan mengembalikan pesan kesalahan.
   
   ```php
   // Validasi format email
   $email = $request->getParam('email');
   if (!preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $email)) {
       $data = [
           'message' => 'Format email tidak valid'
       ];
       return $response->withJson($data, 400);
   }
   ```

2. Built-in Function: Substring (`substr`)
   Menggunakan fungsi `substr` untuk mengambil substring dari parameter `name` yang diberikan, hanya mengambil karakter pertama hingga ke-10 karakter.
   
   ```php
   $name = $request->getParam('name');
   $substring = substr($name, 0, 10);
   ```

3. Built-in Function: Array Manipulation (`array_push`)
   Menggunakan fungsi `array_push` untuk menambahkan elemen baru ke dalam array `$players`. Pada contoh ini, elemen baru ditambahkan setiap kali ada pemain baru yang ditemukan dalam hasil query.
   
   ```php
   $user = [];
   if ($result->num_rows > 0) {
       while ($row = $result->fetch_assoc()) {
           array_push($user, $row);
       }
       return $response->withJson($user, 200);
   }
   ```

## 5
1. Menggunakan subquery untuk mendapatkan total jumlah file dalam setiap folder:
```php
$subquery = "SELECT COUNT(*) as total_files FROM files WHERE folder_id = $folderId";
```
Pada contoh di atas, subquery digunakan untuk menghitung jumlah file dalam setiap folder dengan menggunakan fungsi agregat COUNT.

2. Menggunakan subquery untuk mendapatkan informasi folder dengan total file terbanyak:
```php
$subquery = "SELECT folder_id, folder_name FROM folders WHERE folder_id = (SELECT folder_id FROM files GROUP BY folder_id ORDER BY COUNT(*) DESC LIMIT 1)";
```
Pada contoh ini, subquery digunakan untuk mencari folder dengan total file terbanyak. Subquery tersebut akan mengembalikan folder_id dengan jumlah file terbanyak, kemudian digunakan pada query utama untuk mendapatkan informasi folder.

3. Menggunakan subquery untuk mendapatkan jumlah file dalam folder dengan nama tertentu:
```php
$subquery = "SELECT COUNT(*) as total_files FROM files WHERE folder_id = (SELECT folder_id FROM folders WHERE folder_name = 'Music')";
```
Pada contoh ini, subquery digunakan untuk mencari folder dengan nama 'Music' dan mengambil folder_id-nya. Subquery tersebut kemudian digunakan pada query utama untuk menghitung jumlah file dalam folder tersebut.

4. Menggunakan subquery untuk mendapatkan informasi file dengan ukuran terbesar:
```php
$subquery = "SELECT file_id, file_name FROM files WHERE file_size = (SELECT MAX(file_size) FROM files)";
```
Pada contoh di atas, subquery digunakan untuk mencari ukuran file terbesar dengan fungsi agregat MAX. Subquery tersebut kemudian digunakan pada query utama untuk mendapatkan informasi file dengan ukuran terbesar.

5. Menggunakan subquery untuk mendapatkan informasi file yang terletak dalam folder dengan total file lebih dari 10:
```php
$subquery = "SELECT file_id, file_name FROM files WHERE folder_id IN (SELECT folder_id FROM (SELECT folder_id, COUNT(*) as total_files FROM files GROUP BY folder_id) as subquery WHERE total_files > 10)";
```
Pada contoh ini, subquery digunakan untuk menghitung jumlah file dalam setiap folder dengan subquery terdalam. Subquery tersebut kemudian digunakan pada query utama untuk mendapatkan informasi file yang terletak dalam folder dengan total file lebih dari 10.

## 6
```php
$conn = create_connection();

$conn->begin_transaction();

try {
    // Menambahkan data ke tabel files
    $sql = "INSERT INTO files (file_name, file_size) VALUES (?, ?)";
    $stmt = $conn->prepare($sql);
    
    // Bind parameter
    $stmt->bind_param("si", $file_name, $file_size);
    
    // Set nilai parameter
    $file_name = "example.txt";
    $file_size = 1024;
    
    // Eksekusi statement
    $stmt->execute();
    
    // Mengambil ID file yang baru saja ditambahkan
    $file_id = $conn->insert_id;
    
    // Menambahkan data ke tabel folders
    $sql = "INSERT INTO folders (folder_name, file_id) VALUES (?, ?)";
    $stmt = $conn->prepare($sql);
    
    // Bind parameter
    $stmt->bind_param("si", $folder_name, $file_id);
    
    // Set nilai parameter
    $folder_name = "Folder A";
    
    // Eksekusi statement
    $stmt->execute();
    
    // Commit transaction jika semua operasi berhasil
    $conn->commit();
    
    // Menampilkan pesan berhasil
    echo "Transaksi berhasil. Data telah ditambahkan ke tabel files dan folders.";
} catch (Exception $e) {
    // Rollback transaction jika terjadi kesalahan
    $conn->rollback();
    
    // Menampilkan pesan kesalahan
    echo "Terjadi kesalahan: " . $e->getMessage();
}

// Menutup koneksi
$conn->close();
```

Pada contoh di atas, kita menggunakan transaction untuk menambahkan data ke tabel `files` dan `folders`. Jika kedua operasi berhasil, maka pesan "Transaksi berhasil" akan ditampilkan. Namun, jika terjadi kesalahan, pesan kesalahan akan ditampilkan dan semua perubahan akan dibatalkan.

## 7 
1. Membuat procedure di dalam database:

```sql
CREATE PROCEDURE CountFilesInFolder (IN folder_id INT, OUT file_count INT)
BEGIN
    SELECT COUNT(*) INTO file_count FROM files WHERE folder_id = folder_id;
END
```

2. Menggunakan procedure dalam kode PHP:

```php
// Memulai koneksi ke database
$conn = create_connection();

try {
    // Mengatur mode PDO untuk mengambil hasil output dari procedure
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Menjalankan procedure untuk menghitung jumlah file dalam folder
    $stmt = $conn->prepare("CALL CountFilesInFolder(?, @file_count)");
    $stmt->bindParam(1, $folder_id, PDO::PARAM_INT);
    $stmt->execute();
    
    // Mengambil hasil output dari procedure
    $stmt = $conn->query("SELECT @file_count");
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $file_count = $result['@file_count'];
    
    // Menampilkan hasil
    echo "Jumlah file dalam folder adalah: " . $file_count;
} catch (PDOException $e) {
    // Menampilkan pesan kesalahan
    echo "Terjadi kesalahan: " . $e->getMessage();
}

// Menutup koneksi
$conn = null;
```

Pada contoh di atas, kita menggunakan procedure `CountFilesInFolder` yang menerima parameter `folder_id` sebagai input dan mengembalikan jumlah file dalam folder melalui output parameter `file_count`. Di dalam kode PHP, kita menjalankan procedure menggunakan perintah `CALL` dan mengambil hasil output menggunakan perintah `SELECT @file_count`.

## 8
Data Control Language (DCL) adalah bagian dari SQL yang digunakan untuk mengendalikan hak akses pengguna terhadap database. Berikut adalah contoh penggunaan DCL dalam kode PHP:

1. Grant Privilege:

```php
// Memulai koneksi ke database
$conn = create_connection();

try {
    // Menjalankan perintah GRANT untuk memberikan hak akses pada pengguna
    $sql = "GRANT SELECT, INSERT, UPDATE, DELETE ON files TO 'username'@'localhost'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    
    // Menampilkan pesan berhasil
    echo "Hak akses telah diberikan";
} catch (PDOException $e) {
    // Menampilkan pesan kesalahan
    echo "Terjadi kesalahan: " . $e->getMessage();
}

// Menutup koneksi
$conn = null;
```

Pada contoh di ini, kita menggunakan perintah SQL `GRANT` untuk memberikan hak akses SELECT, INSERT, UPDATE, dan DELETE pada tabel `files` kepada pengguna dengan username 'username' dan host 'localhost'.

2. Revoke Privilege:

```php
// Memulai koneksi ke database
$conn = create_connection();

try {
    // Menjalankan perintah REVOKE untuk mencabut hak akses dari pengguna
    $sql = "REVOKE SELECT, INSERT, UPDATE, DELETE ON files FROM 'username'@'localhost'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    
    // Menampilkan pesan berhasil
    echo "Hak akses telah dicabut";
} catch (PDOException $e) {
    // Menampilkan pesan kesalahan
    echo "Terjadi kesalahan: " . $e->getMessage();
}

// Menutup koneksi
$conn = null;
```

Contoh di ini, kita menggunakan perintah SQL `REVOKE` untuk mencabut hak akses SELECT, INSERT, UPDATE, dan DELETE dari tabel `files` untuk pengguna dengan username `username` dan host `localhost`.

## 9 
Disini saya menggunakan bahasa SQL karena saya memasukan nya saat membuat tabel di dalam database

1. Foreign Key Constraint:
```sql
-- Menambahkan foreign key constraint
ALTER TABLE files
ADD CONSTRAINT fk_files_user_id
FOREIGN KEY (user_id)
REFERENCES users (id);
```

2. Index Constraint:
```sql
-- Menambahkan index constraint
CREATE INDEX idx_users_username
ON users (username);
```

3. Unique Key Constraint:
```sql
-- Menambahkan unique key constraint
ALTER TABLE users
ADD CONSTRAINT uk_users_username
UNIQUE (username);
```
